import provided.*;
import provided.ast.Program;
import student.Parser;
import student.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Parser parser = new Parser(scanner);

        Program p = parser.augmenting();

        System.out.println(JSON.toJson(p));

    }

}
