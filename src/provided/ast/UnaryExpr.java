package provided.ast;
import provided.*;
import java.util.*;

public class UnaryExpr extends Expr {
	public Token op;
	public Expr expr;

	public UnaryExpr(Token op, Expr expr) {
		this.op = op;
		this.expr = expr;
	}
}
