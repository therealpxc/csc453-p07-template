package provided.ast;
import provided.*;
import java.util.*;

public class AndExpr extends Expr {
	public Expr left;
	public Expr right;

	public AndExpr(Expr left, Expr right) {
		this.left = left;
		this.right = right;
	}
}
