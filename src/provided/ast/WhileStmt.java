package provided.ast;
import provided.*;
import java.util.*;

public class WhileStmt extends Stmt {
	public Expr expr;
	public StmtList stmts;

	public WhileStmt(Expr expr, StmtList stmts) {
		this.expr = expr;
		this.stmts = stmts;
	}
}
