package provided.ast;
import provided.*;
import java.util.*;

public class ReturnStmt extends Stmt {
	public Expr expr;

	public ReturnStmt(Expr expr) {
		this.expr = expr;
	}
}
