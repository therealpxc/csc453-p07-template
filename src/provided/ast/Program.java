package provided.ast;
import provided.*;
import java.util.*;

public class Program extends Stmt {
	public StmtList stmts;
	public SymTab symtab;

	public Program(StmtList stmts) {
		this.stmts = stmts;
	}
}
