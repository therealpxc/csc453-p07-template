package provided.ast;
import provided.*;
import java.util.*;

public class AssignStmt extends Stmt {
	public Expr lhs;
	public Expr rhs;

	public AssignStmt(Expr lhs, Expr rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}
}
