package provided.ast;
import provided.*;
import java.util.*;

public class NumExpr extends Expr {
	public Token num;

	public NumExpr(Token num) {
		this.num = num;
	}
}
