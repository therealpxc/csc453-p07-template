package provided.ast;
import provided.*;
import java.util.*;

public class BinaryExpr extends Expr {
	public Token op;
	public Expr left;
	public Expr right;

	public BinaryExpr(Token op, Expr left, Expr right) {
		this.op = op;
		this.left = left;
		this.right = right;
	}
}
