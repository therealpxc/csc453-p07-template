package provided.ast;
import provided.*;
import java.util.*;

public class SymTab {
	public Map<String,Symbol> table;
	public SymTab outer;
	public int level;

	public SymTab(Map<String,Symbol> table, SymTab outer, int level) {
		this.table = table;
		this.outer = outer;
		this.level = level;
	}
}
