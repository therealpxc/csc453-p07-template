package provided.ast;
import provided.*;
import java.util.*;

public class CallExpr extends Expr {
	public Expr fn;
	public List<Expr> args;

	public CallExpr(Expr fn, List<Expr> args) {
		this.fn = fn;
		this.args = args;
	}
}
