package provided.ast;
import provided.*;
import java.util.*;

public class TrueExpr extends Expr {
	public Token val;

	public TrueExpr(Token val) {
		this.val = val;
	}
}
