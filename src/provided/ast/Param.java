package provided.ast;
import provided.*;
import java.util.*;

public class Param {
	public Token id;
	public Symbol sym;

	public Param(Token id) {
		this.id = id;
	}
}
