package provided.ast;
import provided.*;
import java.util.*;

public class IndexExpr extends Expr {
	public Expr expr;
	public Expr index;

	public IndexExpr(Expr expr, Expr index) {
		this.expr = expr;
		this.index = index;
	}
}
