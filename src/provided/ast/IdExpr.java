package provided.ast;
import provided.*;
import java.util.*;

public class IdExpr extends Expr {
	public Token id;
	public Symbol sym;

	public IdExpr(Token id) {
		this.id = id;
	}
}
