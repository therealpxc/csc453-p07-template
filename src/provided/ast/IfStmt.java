package provided.ast;
import provided.*;
import java.util.*;

public class IfStmt extends Stmt {
	public Expr expr;
	public Stmt thenPart;
	public Stmt elsePart;

	public IfStmt(Expr expr, Stmt thenPart, Stmt elsePart) {
		this.expr = expr;
		this.thenPart = thenPart;
		this.elsePart = elsePart;
	}
}
