package provided.ast;
import provided.*;
import java.util.*;

public class DeclStmt extends Stmt {
	public Token id;
	public Symbol sym;

	public DeclStmt(Token id) {
		this.id = id;
	}
}
