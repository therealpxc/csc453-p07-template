package provided.ast;
import provided.*;
import java.util.*;

public class FuncExpr extends Expr {
	public List<Param> params;
	public StmtList stmts;
	public SymTab symtab;

	public FuncExpr(List<Param> params, StmtList stmts) {
		this.params = params;
		this.stmts = stmts;
	}
}
