package provided.ast;
import provided.*;
import java.util.*;

public class OrExpr extends Expr {
	public Expr left;
	public Expr right;

	public OrExpr(Expr left, Expr right) {
		this.left = left;
		this.right = right;
	}
}
