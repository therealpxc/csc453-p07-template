package provided.ast;
import provided.*;
import java.util.*;

public class FalseExpr extends Expr {
	public Token val;

	public FalseExpr(Token val) {
		this.val = val;
	}
}
