package provided.ast;
import provided.*;
import java.util.*;

public class Symbol {
	public SymTab symtab;
	public String id;

	public Symbol(SymTab symtab, String id) {
		this.symtab = symtab;
		this.id = id;
	}
}
