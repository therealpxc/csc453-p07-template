package provided.ast;
import provided.*;
import java.util.*;

public class StmtList extends Stmt {
	public List<Stmt> stmts;
	public SymTab symtab;

	public StmtList(List<Stmt> stmts) {
		this.stmts = stmts;
	}
}
