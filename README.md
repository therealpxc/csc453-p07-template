# wat?
This repository is an example of how to build Java projects using a single, non-recursive Makefile without relying on any unnamed packages.

## if you use direnv
If you have direnv installed and configured for your shell, running `direnv allow .` in the root of the repository will add the `src/` directory and the Google JSON library to the `CLASSPATH`. It will also add bin/json_diff to the `PATH`.

## but don't worry about direnv
If you do not have direnv installed, but you have modified your environment to include the GSON jar file, you can ignore the `.envrc` file.

## to build
To build the project, call `make` from the root directory or `src/`. (The Makefile in the root of the repository is provided only for convenience; it simply calls `src/Makefile`.)
